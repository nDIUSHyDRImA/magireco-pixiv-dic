package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/gocolly/colly"

	"example.com/hello/ent"
	_ "github.com/mattn/go-sqlite3"
)

const characterListUrl = "https://dic.pixiv.net/a/%E3%83%9E%E3%82%AE%E3%82%A2%E3%83%AC%E3%82%B3%E3%83%BC%E3%83%89%E3%81%AE%E3%82%AD%E3%83%A3%E3%83%A9%E3%82%AF%E3%82%BF%E3%83%BC%E4%B8%80%E8%A6%A7"
var client *ent.Client
var ctx context.Context

func main() {
	tmp, err := ent.Open("sqlite3", "file:magireco.sqlite?cache=shared&_fk=1")
    if err != nil {
        log.Fatalf("failed opening connection to sqlite: %v", err)
    }
	client = tmp
	defer client.Close()
	if err := client.Schema.Create(context.Background()); err != nil {
        log.Fatalf("failed creating schema resources: %v", err)
    }
	ctx = context.Background()

	c := colly.NewCollector(
		colly.AllowedDomains("dic.pixiv.net"),
	)

	characters := make(map[string]map[string]string)

	c.OnHTML("table", func(table *colly.HTMLElement) {
		if !isCharacterTable(table) {
			return
		}
		table.ForEach("tr:not(:first-of-type)", func(_ int, row *colly.HTMLElement) {
			characterPageHref, exists := row.DOM.Find("th a").Attr("href")
			if !exists {
				return
			}
			visitCharacterPage(c.Clone(), table.Request.AbsoluteURL(characterPageHref), characters)
		})
	})

	c.Visit(characterListUrl)
}

func isCharacterTable(table *colly.HTMLElement) bool {
	return table.DOM.Find("tr:first-of-type th:first-of-type").Text() == "名前"
}

func visitCharacterPage(c *colly.Collector, url string, characters map[string]map[string]string) {
	var character *ent.Character
	var characterName string
	pageCollector := c.Clone()

	pageCollector.OnHTML("#article-name", func(h *colly.HTMLElement) {
		characterName = h.Text
		characters[characterName] = make(map[string]string)
		tmp, _ := client.Character.Create().SetName(characterName).Save(ctx)
		character = tmp
	})

	pageCollector.OnHTML("table:first-of-type", func(table *colly.HTMLElement) {
		table.ForEach("tr", func(_ int, row *colly.HTMLElement) {
			characters[characterName][row.ChildText("th")] = row.ChildText("td")
			err := client.CharacterInfo.Create().SetTitle(row.ChildText("th")).SetContent(row.ChildText("td")).SetCharacter(character).Exec(ctx)
			if err != nil {
				panic(err)
			}
		})
		fmt.Println(characters)
	})

	pageCollector.OnResponse(func(r *colly.Response) {
		time.Sleep(time.Second * 1)
	})

	pageCollector.Visit(url)
}
