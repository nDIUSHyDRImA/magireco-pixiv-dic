package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// CharacterInfo holds the schema definition for the CharacterInfo entity.
type CharacterInfo struct {
	ent.Schema
}

// Fields of the CharacterInfo.
func (CharacterInfo) Fields() []ent.Field {
	return []ent.Field{
		field.String("title").NotEmpty(),
		field.String("content").NotEmpty(),
	}
}

// Edges of the CharacterInfo.
func (CharacterInfo) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("character", Character.Type).Ref("infos").Unique(),
	}
}
